/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda;

import java.util.Vector;

public class Agenda {
    
    private Vector<Pessoa> listaContatos;
    
    public Agenda() {
        listaContatos = new Vector<Pessoa>();
    }
   
    public String adicionarContato(PessoaFisica contatoPessoaFisica){
        listaContatos.add(contatoPessoaFisica);
        return "Contato Cadastrado com Sucesso,"+contatoPessoaFisica.getNome()+", uma pessoa física";
    }
    
    public String adicionarContato(PessoaJuridica contatoPessoaJuridica){
        listaContatos.add(contatoPessoaJuridica);
        return "Contato Cadastrado com Sucesso,"+contatoPessoaJuridica.getNome()+", uma pessoa juridica";
    }
}

