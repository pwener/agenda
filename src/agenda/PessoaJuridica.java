package agenda;

public class PessoaJuridica extends Pessoa {

	private String cpnj;
        private String razaoSocial;

	public PessoaJuridica(String nome) {
		super(nome);
	}

	public String getCpnj() {
		return cpnj;
	}

	public void setCpnj(String cpnj) {
		this.cpnj = cpnj;
	}

        public String getRazaoSocial() {
           return razaoSocial;
       }

        public void setRazaoSocial(String razaoSocial) {
             this.razaoSocial = razaoSocial;
         }

}
